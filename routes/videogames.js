const express = require('express');
const router = express.Router();
const videogameController = require('../app/api/controllers/videogames');

router.get('/', videogameController.getAll);
router.post('/', videogameController.create);
router.get('/:videogameId', videogameController.getById);
router.put('/:videogameId', videogameController.updateById);
router.delete('/:videogameId', videogameController.deleteById);

module.exports = router;